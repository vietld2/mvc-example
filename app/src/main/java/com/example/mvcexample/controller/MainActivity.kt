package com.example.mvcexample.controller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import com.example.mvcexample.R
import com.example.mvcexample.controller.adapter.CountryAdapter
import com.example.mvcexample.model.Country

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()

        val listCountry = mutableListOf<Country>()

        val countryAdapter = CountryAdapter(this)
        rvCountry?.adapter = countryAdapter

        btnAdd?.setOnClickListener {
            val country = Country(
                edCountryName?.text.toString(),
                edCapital?.text.toString()
            )

            listCountry.add(country)
            countryAdapter.submitList(listCountry)
            countryAdapter.notifyDataSetChanged()
        }

        btnFilter?.setOnClickListener {
            val newList = listCountry.filter {
                it.capital.isNotEmpty() && it.name.isNotEmpty()
            }
            countryAdapter.submitList(newList)
            countryAdapter.notifyDataSetChanged()
        }
    }


    private var edCountryName: EditText? = null
    private var edCapital: EditText? = null
    private var rvCountry: ListView? = null
    private var btnAdd: Button? = null
    private var btnFilter: Button? = null

    private fun initView() {
        edCountryName = findViewById<EditText>(R.id.ed_countryName)
        edCapital = findViewById<EditText>(R.id.ed_capital)
        rvCountry = findViewById<ListView>(R.id.rv_country)
        btnAdd = findViewById<Button>(R.id.btn_add)
        btnFilter = findViewById<Button>(R.id.btn_filter)
    }
}